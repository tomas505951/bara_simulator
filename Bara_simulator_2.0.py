import math
import random
import pyglet

Time =0
direction = 1
direction2 = -1
score = 0
variable = random.randint(1,100)

window = pyglet.window.Window(resizable = True, fullscreen = True)
pyglet.gl.glClearColor(1,1,1,1)

music = pyglet.resource.media('song1.mp3')
music.loop = True
music.play()


had = pyglet.sprite.Sprite(pyglet.image.load('ovce.png'), x=0, y=0)
 
mys = pyglet.sprite.Sprite(pyglet.image.load('bara.jpg'), x=300, y=300)

victory = pyglet.sprite.Sprite(pyglet.image.load('boy.jpg'), x=200, y=50)

victory2 = pyglet.sprite.Sprite(pyglet.image.load('girl.png'), x=300, y=100)

victory3 = pyglet.sprite.Sprite(pyglet.image.load('bj.jpg'), x=300, y=30)

victory4 = pyglet.sprite.Sprite(pyglet.image.load('fast.jpg'), x=250, y=50)

chad = pyglet.sprite.Sprite(pyglet.image.load('Chad.jpg'), x = random.randint(1,1400), y=random.randint(1,900))

cheat = pyglet.sprite.Sprite(pyglet.image.load('cheat.jpg'), x = 100, y=100)

fight = pyglet.sprite.Sprite(pyglet.image.load('fight.png'), x = 100, y=20)

hory = pyglet.sprite.Sprite(pyglet.image.load('hory.jpg'), x = 0, y=400)

end = pyglet.sprite.Sprite(pyglet.image.load('end.jpg'), x = 300, y=300)

def metric(A, B):
    return math.sqrt(((abs((A.x-B.x))^2)+(abs((A.y-B.y))^2)))

def tik(t):
    
    step = (50 * (score // 1000)) + 50
    if step < 25: step = 25
    
    global Time
    Time +=1
    
    if Time % 30 == 0:
        global direction
        direction = random.choice([1,-1])
        global direction2
        direction2 = random.choice([1,-1])

    if Time % 100 == 0 and metric(mys,chad) >= 10 and metric(had, chad) > 10:
        chad.x = random.randint(10,1400)
        chad.y = random.randint(10,900)

    if (metric(mys, had) > 10 and metric(mys, chad) >= 10 and metric(chad, had) > 10)\
       or (metric(had, mys) < 10 and (had.x > hory.x and had.y > hory.y and had.y < hory.y + 450 and had.x < hory.x + 744)):
        mys.x += (round(t*step)*direction)
        mys.y += (round(t*step)*direction2)
        
    if mys.x >1500:
        mys.x = 0
    elif mys.x <0:
        mys.x = 200
    elif mys.y > 900:
        mys.y = 0
    elif mys.y < 0:
        mys.y = 200

def vykresli():

    if (score < 1000):
        text = 'Skóre: ' + str(score)
    else:
        text = 'Skóre: ' + str(score) + '*'
        
    label = pyglet.text.Label(text,
                          font_name='Times New Roman',
                          font_size=60, color = (0,0,0,255),
                          x=1100, y=770)
    window.clear()
    
    if (metric(mys, had) > 10 and metric(mys, chad) >= 10 and metric(chad, had) > 10)\
        or (metric(mys, had) < 10 and (had.x > hory.x and had.y > hory.y and had.y < hory.y+ 450 and had.x < hory.x + 744)):
        hory.draw()
        had.draw() 
        mys.draw()
        chad.draw()
        
    elif metric(mys, chad) < 10:
        cheat.draw()

        @window.event
        def on_key_press(symbol, modifier):
            if metric(mys, chad) < 10 and symbol == pyglet.window.key.ENTER:
                chad.x = mys.x + random.randint(-600, 600)
                global score
                score -= 200

    elif metric(mys, had) > 10 and metric(mys, chad) >= 10 and metric(chad, had) <= 10:
            fight.draw()

            @window.event
            def on_key_press(symbol, modifier):
                if metric(chad, had) <= 10 and symbol == pyglet.window.key.ENTER:
                    chad.x = had.x + random.randint(-600, 600)
                    global score
                    score -= 150
        
    else:
        if not (metric(mys, had) < 10 and (had.x > hory.x and had.y > hory.y and had.y < hory.y + 450 and had.x < hory.x + 744)):
            if variable >= 60:
                victory.draw()
                gains = 150
            elif variable >= 20 and variable < 60:
                victory2.draw()
                gains = - 100
            elif variable >= 10 and variable < 20:
                victory3.draw()
                gains = 300
            else:
                victory4.draw()
                gains = -50
        
            @window.event
            def on_key_press(symbol, modifier):
                if metric(mys, had) <= 10 and symbol == pyglet.window.key.ENTER:
                    mys.x = had.x + random.randint(-600, 600)
                    global score
                    score += gains

                    global variable
                    variable = random.randint(1,100)
                
    label.draw()
  
def klik(x, y, tlacitko, mod):
    if (metric(mys, had) > 10 and metric(mys, chad) >= 10) or (metric(mys, had) < 10 and (had.x > hory.x and had.y > hory.y and had.y < hory.y + 450 and had.x < hory.x + 744)):
        had.x = x
        had.y = y

pyglet.clock.schedule_interval(tik, 1/30)
    
window.push_handlers(
    on_draw=vykresli,
    on_mouse_press=klik,
)

pyglet.app.run()
